﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using CameraModelName = System.String;
using ProtocolName = System.String;
using ImageFormatName = System.String;
using UrlPart = System.String;

namespace Camurl
{

     public partial class Form1 : Form
     {
          public Form1()
          {
               InitializeComponent();
          }

          private void Form1_Load(object sender, EventArgs e)
          {
               ReloadData();
          }

          public void ReloadData()
          {
               try
               {
                    if (System.IO.Directory.Exists("data"))
                    {
                         System.IO.Directory.Delete("data", true);
                    }
               }
               catch (Exception)
               {

               }

               try
               {
                    System.IO.Directory.CreateDirectory("data");
                    System.IO.File.WriteAllBytes("data/data.zip", Properties.Resources.data);
                    System.IO.Compression.ZipFile.ExtractToDirectory("data/data.zip", "data");
                    System.IO.File.Delete("data/data.zip");
               }
               catch (Exception ex)
               {
                    MessageBox.Show(ex.ToString());
               }

               try
               {
                    var brandsFile = System.IO.File.ReadAllText("data/brands.json");
                    var brands = Newtonsoft.Json.JsonConvert.DeserializeObject<List<String>>(brandsFile);
                    brandCbx.DataSource = brands;
               }
               catch (Exception)
               {

               }
          }

          private void findBtn_Click(object sender, EventArgs e)
          {
               try
               {
                    var brand = brandCbx.SelectedItem.ToString();
                    var ip = edtIp.Text;
                    var channel = edtChannel.Text;
                    var login = edtLogin.Text;
                    var pwd = edtPwd.Text;
                    var cameraFileName = "data/cameras/" + brand + ".json";
                    var cameraFile = System.IO.File.ReadAllText(cameraFileName );
                    var camera = Newtonsoft.Json.JsonConvert.DeserializeObject<Dictionary<ProtocolName, ProtocolInfo> >(cameraFile);
                    System.Collections.Generic.List<String> data = new List<String>();
                    foreach ( var protocol in camera )
                    {
                         foreach (var imageType in protocol.Value.types )
                         {
                              foreach (var url in imageType.Value )
                              {
                                   data.Add( replacePlaceholders( protocol.Key + edtIp.Text + url.Key) );

                                   foreach (var subUrl in url.Value)
                                   {
                                        data.Add( replacePlaceholders( protocol.Key + edtIp.Text + url.Key + "?" + subUrl ) );
                                   }
                              }
                         }
                    }
                    resultBox.Lines = data.ToArray();
               }
               catch (Exception ex)
               {
                    MessageBox.Show(ex.ToString());
               }
          }

          public string replacePlaceholders(string text)
          {
               return text.Replace("[USERNAME]", edtLogin.Text).Replace("[PASSWORD]", edtPwd.Text).Replace("[CHANNEL]", edtChannel.Text)
                    .Replace("[WIDTH]", "640" )
                    .Replace("[HEIGHT]", "480" );
          }
     }

     public class ProtocolInfo
     {
          public List<String> models;
          public Dictionary<ImageFormatName, Dictionary<UrlPart, List<UrlPart>>> types;
     }
}
