## Camurl

Инструкция по использованию:
1. Выбираете вендора/модель камеры из списка
2. Вписываете IP
3. Вписываете предполагаемые логин/пароль/канал
4. В текстовом поле появляются ссылки на трансляции с камер этих моделей
5. Проверяете ссылки в браузере/плеере

Инструкция по сборке:
1. Установить .NET 4.5 (сборка с более ранними версиями .NET не проверялась) либо Mono Framework на linux-подобных системах.
2. Опционально. Установите Visual Studio (автором использовалась Visual Studio 2015), поддерживающую разработку в среде .NET
3. Установить пакет Newtonsoft Json.NET с помощью Nuget Package Manager (встроен в Visual Studio последних версий), 
либо вручную скачать с официального сайта: https://www.newtonsoft.com/json, если VS не используется
4. При необходимости обновить файл с даными камер data.zip из архива ссылок.

Архив ссылок взят из репозитория ShinobiCCTV/cameraConnectionList
([GitHub](https://github.com/ShinobiCCTV/cameraConnectionList), 
[GitLab](https://gitlab.com/Shinobi-Systems/cameraConnectionList))

![](window.png)